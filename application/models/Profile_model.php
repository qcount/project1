<?php

class Profile_model extends CI_Model {

	public function __construct()
    {
    	parent::__construct();
	}

	public function get_all()
    {
    	$query = $this->db->get('identitas');
        return $query->result_array();
	}

}