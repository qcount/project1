<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('profile_model','identitas');
	}

	public function index()
	{
		$dataprofil = $this->identitas->get_all();
		$this->load->view('layout', array(
			'content'=>'profile/index.php',
			'daftar'=>$dataprofil
		));
	}

}
